import React from 'react';
import {StyleSheet, StatusBar} from 'react-native';
import UserStore from './src/components/context/stores/UserStore';
import Navigation from './src/components/navigation/StackNavigation';
import {COLORS} from './src/constants';

const App = () => (
  <UserStore>
    <StatusBar barStyle="light-content" backgroundColor={COLORS.background} />
    <Navigation />
  </UserStore>
);
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});

export default App;
