import React, {useContext, useEffect, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {SafeAreaView} from 'react-native-safe-area-context';
import EncryptedStorage from 'react-native-encrypted-storage';

import {UserContext} from '../context/stores/UserStore';
// Screens
import SplashScreen from '../screens/SplashScreen';
import HomePage from '../screens/Home/HomePage';
import AppIntro from '../screens/authentication/AppIntro';
import Login from '../screens/authentication/Login';

const Stack = createNativeStackNavigator();

const AuthScreen = () => (
  <Stack.Navigator
    screenOptions={{headerShown: false}}
    initialRouteName={'AppIntro'}>
    <Stack.Screen name="AppIntro" component={AppIntro} />
    <Stack.Screen name="Login" component={Login} />
  </Stack.Navigator>
);

const MainScreen = () => (
  <Stack.Navigator screenOptions={{headerShown: false}} initialRouteName="Home">
    <Stack.Screen name="Home" component={HomePage} />
  </Stack.Navigator>
);

export default function StackNavigation() {
  const {userState, userDispatch} = useContext(UserContext);
  const [isSplash, setIsSplash] = useState(true);

  const check_profile = async () => {
    const is_slider_done = await EncryptedStorage.getItem('is_slider_done');
    const user = await EncryptedStorage.getItem('user');

    if (is_slider_done) {
      if (user) {
        userDispatch({
          type: 'UPDATE_USER',
          user: {
            is_slider_done: true,
            user: true,
          },
        });
      } else {
        userDispatch({
          type: 'UPDATE_USER',
          user: {
            is_slider_done: true,
          },
        });
      }
    } else {
      console.log('No get user local');
    }
  };

  useEffect(() => {
    check_profile();
    setTimeout(() => {
      setIsSplash(false);
    }, 1500);
  }, []);

  return (
    <NavigationContainer>
      {isSplash ? (
        <SplashScreen />
      ) : !userState.user ? (
        <MainScreen />
      ) : (
        <AuthScreen />
      )}
    </NavigationContainer>
  );
}
