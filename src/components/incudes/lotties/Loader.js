import React from 'react';
import LottieView from 'lottie-react-native';

export default function Loader() {
  return (
    <LottieView
      style={{
        display: 'flex',
        alignItems: 'center',
      }}
      source={require('../../../../assets/lotties/farmers.json')}
      autoPlay
      loop
      // width={170}
    />
  );
}
