import React from 'react';
import LottieView from 'lottie-react-native';

export default function ButtonLoader() {
  return (
    <LottieView
      style={{
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        width: 35,
      }}
      source={require('../../../../assets/lotties/spinner.json')}
      autoPlay
      loop
    />
  );
}
