import React, {useEffect} from 'react';
import {TouchableOpacity, StyleSheet, View, Text, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {COLORS, FONTS} from '../../../constants';

// Svg Icons
import ThemeBack from '../../../assets/icons/theme-back.svg';

export default function Header({
  isPadding,
  isCart,
  isHome,
  title,
  isBack,
  place,
  requestLocationPermission,
}) {
  const navigation = useNavigation();
  useEffect(() => {}, []);

  return (
    <View style={[styles.container, {paddingTop: isPadding && 20}]}>
      <TouchableOpacity onPress={() => navigation.goBack()} activeOpacity={0.8}>
        <ThemeBack height="18px" width="18px" fill={'#fff'} />
      </TouchableOpacity>
      <Text style={styles.middleText}>{title}</Text>
      <View />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 19,
    paddingVertical: 17,
    display: 'flex',
    flexDirection: 'row',
    // justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0)',
  },
  locationWrap: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  icon: {
    position: 'relative',
  },
  count: {
    height: 17,
    width: 17,
    borderRadius: 17 / 2,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: COLORS.danger,
    position: 'absolute',
    right: -4,
    top: -4,
  },
  countText: {
    color: '#fff',
    fontSize: 12,
  },
  middleText: {
    ...FONTS.text2,
    fontSize: 20,
    lineHeight: 20 + 5,
    // textTransform: 'uppercase',
    marginLeft: 15,
  },
  location: {
    ...FONTS.h2,
    color: COLORS.primary,
    // fontSize: 16,
    marginLeft: 6,
  },
  logo: {
    height: 27,
    width: 27,
  },
});
