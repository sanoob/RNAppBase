import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Loader from 'react-native-easy-content-loader';

export default function LoaderContent({ loading, color }) {
  return (
    <View style={{ marginHorizontal: 20, marginTop: 20 }}>
      <Loader
        loading={loading}
        primaryColor="#d2d2d2"
        secondaryColor="#c2c2c2"
        animationDuration={1000}></Loader>
      <View
        style={{
          // marginBottom: 30,
          display: loading ? 'flex' : 'none',
          height: 20,
        }}
      />
      <Loader
        loading={loading}
        primaryColor="#d2d2d2"
        secondaryColor="#c2c2c2"
        animationDuration={1000}></Loader>
      <View
        style={{
          // marginBottom: 30,
          display: loading ? 'flex' : 'none',
          height: 20,
        }}
      />
      <Loader
        loading={loading}
        primaryColor="#d2d2d2"
        secondaryColor="#c2c2c2"
        animationDuration={1000}></Loader>
      <View
        style={{
          // marginBottom: 30,
          display: loading ? 'flex' : 'none',
          height: 20,
        }}
      />
      <Loader
        loading={loading}
        primaryColor="#d2d2d2"
        secondaryColor="#c2c2c2"
        animationDuration={1000}></Loader>
      <View
        style={{
          // marginBottom: 30,
          display: loading ? 'flex' : 'none',
          height: 20,
        }}
      />
      <Loader
        loading={loading}
        primaryColor="#d2d2d2"
        secondaryColor="#c2c2c2"
        animationDuration={1000}></Loader>
      <View
        style={{
          // marginBottom: 30,
          display: loading ? 'flex' : 'none',
          height: 20,
        }}
      />
      <Loader
        loading={loading}
        primaryColor="#d2d2d2"
        secondaryColor="#c2c2c2"
        animationDuration={1000}></Loader>
      <View
        style={{
          // marginBottom: 30,
          display: loading ? 'flex' : 'none',
          height: 20,
        }}
      />
    </View>
  );
}

const styles = StyleSheet.create({});
