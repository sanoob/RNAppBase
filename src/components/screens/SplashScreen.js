import React from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  Dimensions,
  StatusBar,
} from 'react-native';
import {COLORS} from '../../constants';
let {width, height} = Dimensions.get('window');

export default function SplashScreen() {
  return (
    <View style={styles.container}>
      <StatusBar backgroundColor={COLORS.background} barStyle="light-content" />
      {/* <Image
        style={styles.image}
        source={require('../../assets/images/logo.png')}
      /> */}
    </View>
  );
}
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height: height,
    width: width,
    // borderWidth: 2,
    backgroundColor: COLORS.black,
    // COLORs
  },
  image: {
    height: width / 2,
    // width: (width / 2) * 0.59,
    marginBottom: '20%',
    resizeMode: 'contain',
  },
});
