import {StyleSheet, Text, View} from 'react-native';
import React, {useEffect, useState} from 'react';
import {useNavigation} from '@react-navigation/native';

export default function HomePage() {
  let navigation = useNavigation();
  useEffect(() => {
    // Your useEffect code goes here
  }, []);

  return (
    <View style={styles.container}>
      <Text style={styles.headerText}>HomePage</Text>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  headerText: {
    marginTop: 14,
    fontFamily: 'JostBold',
    fontSize: 18,
    marginLeft: 14,
  },
});
