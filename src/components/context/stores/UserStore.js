import React, { createContext, useReducer } from 'react';
import UserReducer from '../reducers/UserReducer';

const initialState = {
  firstName: '',
  lastName: '',
  phone: '',
  email: '',
  user: false,
  image: '',
  is_slider_done: false,
  address: null,
  user: false,
  active_address: false,
};

export const UserContext = createContext(initialState);
const UserStore = ({ children }) => {
  console.log('start');
  const [userState, userDispatch] = useReducer(UserReducer, initialState);
  console.log(userState, 'userStatePPPP');

  return (
    <UserContext.Provider value={{ userState, userDispatch }}>
      {children}
    </UserContext.Provider>
  );
};

export default UserStore;
