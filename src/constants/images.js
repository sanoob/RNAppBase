const placeHolder = require('../assets/images/place_holder.png');
const app_intro = require('../assets/images/authentication/app_intro.png');
const login = require('../assets/images/authentication/login.png');
const otp = require('../assets/images/authentication/otp.png');
const home_banner = require('../assets/images/home/home_banner.png');

// list array images
const service_image_list = {
  // painting: require('../assets/images/PaintingIcon.png'),
  // plumbing: require('../assets/images/Plumbing.png'),
  // roofing: require('../assets/images/RoofingIcon.png'),
};

export default {
  app_intro,
  login,
  otp,
  home_banner,
};
