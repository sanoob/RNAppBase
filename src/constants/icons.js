export const Notification = require('../assets/icons/notification.svg');
export const menu = require('../assets/icons/menu.svg');
export const ThemeHeart = require('../assets/icons/theme-heart.svg');
export const ThemeSearch = require('../assets/icons/theme-search.svg');
export const ThemeBack = require('../assets/icons/theme-back.svg');

export default {
  Notification,
  menu,
  ThemeHeart,
  ThemeSearch,
  ThemeBack,
};
