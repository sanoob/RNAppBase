// import icons from './icons';
// import images from './images';
import BASE_URL from './BaseUrl';
import theme, { COLORS, SIZES, FONTS } from './theme';

export { theme, COLORS, SIZES, FONTS, BASE_URL };
