import {COLOR} from '@interfaces/Enums';

const COLORS = [
  COLOR.lightBlue,
  COLOR.lightOrange,
  COLOR.lightPink,
  COLOR.lightPurple,
  COLOR.yellow,
];

const randomColor = (): COLOR => {
  const index = Math.floor(Math.random() * COLORS.length);
  return COLORS[index];
};

export default randomColor;
