import {postFile} from '@helpers/fetch';
import {launchImageLibrary} from 'react-native-image-picker';

const readFileAsBlob = async (filePath: string) => {
  const response = await fetch(filePath);
  return await response.blob();
};

const callback = async (res: any, setter: (value: string) => void) => {
  if (res.didCancel) return;

  const {uri, type} = res;
  setter(uri);

  const data = await readFileAsBlob(uri);

  const response = await postFile(data, type);
  const {url} = await response.json();
  setter(url);
};

const onPressUpload = (setter: (value: string) => void) =>
  launchImageLibrary({mediaType: 'photo'}, (res: any) => callback(res, setter));

export default onPressUpload;
