const formatTimestamp = (timestamp: number): string => {
  let midnight = new Date();
  midnight.setHours(0, 0, 0, 0);

  if (timestamp < midnight.getTime()) {
    return new Date(timestamp).toLocaleDateString('en', {
      day: 'numeric',
      month: 'numeric',
      year: '2-digit',
    });
  }
  return new Date(timestamp).toLocaleTimeString([], {
    hour: '2-digit',
    minute: '2-digit',
  });
};

export default formatTimestamp;
