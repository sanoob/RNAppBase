export const formatPhoneUIForm = (phone: string): string => {
  if (phone.length > 14) return phone.substring(0, 14);

  if (phone.length === 4 && phone[0] !== '(') {
    phone = `(${phone.substring(0, 3)}) ${phone[3]}`;
  } else if (phone.length === 10 && phone[9] !== '-') {
    phone = `${phone.substring(0, 9)}-${phone[9]}`;
  } else if (phone.length <= 14) {
    phone = phone;
  }

  return phone;
};

export const formatPhoneFromBackend = (phone: string): string => {
  return `(${phone.substring(2, 5)}) ${phone.substring(5, 8)}-${phone.substring(
    8,
    12,
  )}`;
};

export const formatPhoneToBackend = (phone: string): string => {
  return `+1${phone?.substring(1, 4)}${phone?.substring(
    6,
    9,
  )}${phone?.substring(10, 14)}`;
};
