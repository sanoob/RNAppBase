const sortTimestamp = <T>(
  arr: Array<T & {timestamp: number}> = [],
): Array<T> => {
  const copy = [...arr];
  copy.sort((a, b) => b.timestamp - a.timestamp);
  return copy;
};

export default sortTimestamp;
