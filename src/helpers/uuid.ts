import * as ReactNativeUuid from 'react-native-uuid';

const uuid = (): string => {
  return ReactNativeUuid.default.v4() as string;
};

export default uuid;
