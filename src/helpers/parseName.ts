import {parseFullName} from 'parse-full-name';

const parseName = (
  firstName?: string,
  lastName?: string,
): {
  firstName?: string;
  lastName?: string;
  name?: string;
  initials?: string;
} => {
  if (firstName === undefined || lastName === undefined) return {};

  const parsedName = parseFullName(`${firstName} ${lastName}`, 'all', true);

  return {
    firstName: parsedName.first,
    lastName: parsedName.last,
    name:
      parsedName === undefined
        ? undefined
        : `${parsedName.first} ${parsedName.last}`,
    initials:
      parsedName.first === undefined || parsedName.last === undefined
        ? undefined
        : `${parsedName.first[0]?.toUpperCase() ?? ''}${
            parsedName.last[0]?.toUpperCase() ?? ''
          }`,
  };
};

export default parseName;
