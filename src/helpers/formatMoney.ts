const formatMoney = (money: number, truncate?: boolean): string | undefined => {
  const formatter = new Intl.NumberFormat('en-US', {
    //   style: 'currency',
    currency: 'USD',
    maximumFractionDigits: truncate ? 0 : 2,
  });

  if (!money) return undefined;
  return formatter.format(money);
};

export default formatMoney;
