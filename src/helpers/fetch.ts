import {authHeader} from './token';

// const rootUrl = 'https://api.dev.getsymba.com';

export const post = async (
  path: string,
  isAuthed: boolean = false,
  body?: object,
) => {
  let headers = {};

  if (body) {
    headers = {...headers, 'Content-Type': 'application/json'};
  }

  if (isAuthed) {
    const bearer = await authHeader();
    headers = {...headers, ...bearer};
  }

  let req: RequestInit = {method: 'POST', headers};

  if (body) req = {...req, body: JSON.stringify(body)};

  try {
    return await fetch(`${rootUrl}${path}`, req);
  } catch {
    return {status: 500, json: async () => {}};
  }
};

export const put = async (
  path: string,
  isAuthed: boolean = false,
  body?: object,
) => {
  let headers = {};

  if (body) {
    headers = {...headers, 'Content-Type': 'application/json'};
  }

  if (isAuthed) {
    const bearer = await authHeader();
    headers = {...headers, ...bearer};
  }

  let req: RequestInit = {method: 'PUT', headers};

  if (body) req = {...req, body: JSON.stringify(body)};

  try {
    return await fetch(`${rootUrl}${path}`, req);
  } catch {
    return {status: 500};
  }
};

export const postFile = async (data: any, type: string): Promise<Response> => {
  const bearer = await authHeader();
  const headers = {'Content-Type': type, ...bearer};

  const req: RequestInit = {
    method: 'POST',
    headers,
    body: data,
  };

  return await fetch(`${rootUrl}/file`, req);
};

export const get = async (path: string, isAuth: boolean = false, isBaseUrl: boolean = true) => {
  const headers = isAuth ? await authHeader() : {};

  return await fetch(`${isBaseUrl?rootUrl:''}${path}`, {method: 'GET', headers});
};


export const del = async (
  path: string,
  isAuthed: boolean = false,
  body?: object,
) => {
  let headers = isAuthed ? await authHeader() : {};

  if (body) {
    headers = {...headers, 'Content-Type': 'application/json'};
  }

  let req: RequestInit = {method: 'DELETE', headers};

  if (body) req = {...req, body: JSON.stringify(body)};

  return await fetch(`${rootUrl}${path}`, req);
};
