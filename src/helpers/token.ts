// import AsyncStorage from '@react-native-async-storage/async-storage';

const LS_TOKEN = 'sym-tok';
const LS_TOKEN_RFR = 'sym-tok-rfr';
const LS_TOKEN_EXP = 'sym-tok-exp';
// const rootUrl = process.env.NEXT_PUBLIC_API_ROOT_URL;

let cachedToken: string | null = null;
let cachedRefreshToken: string | null = null;
let cachedExpiration: number | null = null;

export const signOut = async () => {
  await AsyncStorage.removeItem(LS_TOKEN);
  await AsyncStorage.removeItem(LS_TOKEN_RFR);
  await AsyncStorage.removeItem(LS_TOKEN_EXP);
};

export const saveToken = async (
  token: string,
  refreshToken: string,
  expiration: number,
) => {
  cachedToken = token;
  await AsyncStorage.setItem(LS_TOKEN, cachedToken);

  cachedRefreshToken = refreshToken;
  await AsyncStorage.setItem(LS_TOKEN_RFR, cachedRefreshToken);

  cachedExpiration = expiration;
  await AsyncStorage.setItem(LS_TOKEN_EXP, String(cachedExpiration));
};

const refreshToken = async () => {
  //   if (!cachedRefreshToken && !localStorage.getItem(LS_TOKEN_RFR))
  //     throw new Error("Can't refresh, refresh token not defined");

  //   if (!cachedRefreshToken)
  //     cachedRefreshToken = localStorage.getItem(LS_TOKEN_RFR);

  //   const response = await fetch(`${rootUrl}/token/refresh`, {
  //     method: 'POST',
  //     headers: {'Content-Type': 'application/json'},
  //     body: JSON.stringify({refreshToken: cachedRefreshToken}),
  //   });

  //   if (response.status !== 200) return false;

  //   const {token, expiration} = await response.json();

  //   if (!token || !expiration) return false;

  //   cachedToken = token;
  //   localStorage.setItem(LS_TOKEN, cachedToken);

  //   cachedExpiration = expiration;
  //   localStorage.setItem(LS_TOKEN_EXP, cachedExpiration);

  return true;
};

const hasToken = async (): Promise<boolean> => {
  if (cachedToken) return true;

  cachedToken = await AsyncStorage.getItem(LS_TOKEN);

  return cachedToken !== null;
};

const shouldRefresh = async (): Promise<boolean> => {
  const exp = await AsyncStorage.getItem(LS_TOKEN_EXP);
  if (!cachedExpiration && !exp) signOut();

  if (!cachedExpiration) cachedExpiration = Number(exp);

  const fiveMinuteBuffer = Date.now() + 1000 * 60 * 5;
  return cachedExpiration < fiveMinuteBuffer;
};

export const authHeader = async () => {
  if (await shouldRefresh()) await refreshToken();

  if (!(await hasToken())) throw new Error('Token is not defined');

  return {
    Authorization: `Bearer ${cachedToken}`,
  };
};

export const isSignedIn = async (): Promise<boolean> => {
  if (!(await hasToken())) return false;

  if (await shouldRefresh()) {
    const refreshed = await refreshToken();
    if (!refreshed) return false;
  }

  return true;
};
