import {NOTIFICATION_TYPE} from '@interfaces/Enums';
import PushNotificationIOS from '@react-native-community/push-notification-ios';
import uuid from './uuid';

// NOTE: not in use, going with remote notifications instead
export const fireTaskNotification = (
  taskId: string,
  task: string,
  notes?: string,
) => {
  PushNotificationIOS.addNotificationRequest({
    id: uuid(),
    title: 'Task Due',
    subtitle: task,
    body: notes,
    userInfo: {
      type: NOTIFICATION_TYPE.task,
      taskId,
    },
  });
};
